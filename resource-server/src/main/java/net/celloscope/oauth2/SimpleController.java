package net.celloscope.oauth2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;

@RestController
public class SimpleController {

    @GetMapping("/whoami")
    public String whoami(@AuthenticationPrincipal Principal principal) {
        System.out.println(principal.getName());
        return principal.getName();
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello World";
    }

    @RequestMapping("/all")
    public String permitAll() {
        return "Hello for all users.";
    }

    /*@GetMapping("/get-token")
    public Object getToken(@RequestParam(value = "code", required = true) String code)  throws JsonProcessingException, IOException {
        System.out.println("Code get form oauth server --------------- : " + code);
        System.out.println("Authorization Ccode------" + code);

        ResponseEntity<String> response = null;
        RestTemplate restTemplate = new RestTemplate();

        String credentials = "auth-grant-client:auth-grant-secret";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Basic " + encodedCredentials);

        HttpEntity<String> request = new HttpEntity<String>(headers);

        String access_token_url = "http://localhost:8080/oauth/token";
        access_token_url += "?code=" + code;
        access_token_url += "&grant_type=authorization_code";
        access_token_url += "&redirect_uri=http://localhost:9090/get-token";

        response = restTemplate.exchange(access_token_url, HttpMethod.POST, request, String.class);

        System.out.println("Access Token Response ---------" + response.getBody());

        // Get the Access Token From the recieved JSON response
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(response.getBody());
        String token = node.path("access_token").asText();

        System.out.println("Token got form oauth server ----------- : " + token);
        return response.getBody();
    }*/

}
