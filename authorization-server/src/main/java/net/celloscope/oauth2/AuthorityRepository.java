package net.celloscope.oauth2;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<AuthorityEntity, Integer> {

    @NotFound(action = NotFoundAction.IGNORE)
    @Query(value = "SELECT distinct a.authorityOid, a.authorityName FROM AUTHORITY a " +
            "INNER JOIN RoleAuthority ra ON a.authorityOid=ra.authorityId " +
            "INNER JOIN ROLE r ON r.roleOid=ra.roleId " +
            "INNER JOIN UserRole ur ON ur.roleId=r.roleOid " +
            "INNER JOIN ibuser u ON u.ibuseroid=ur.userId " +
            "WHERE u.userid = ?1",
            nativeQuery = true)
    List<AuthorityEntity> getByUserId(String userid);



}
