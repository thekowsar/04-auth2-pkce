package net.celloscope.oauth2;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Role")
public class RoleEntity {

    @Id
    @Column(name = "roleoid")
    private Integer roleOid;

    @Column(name="rolename")
    private String roleName;

}
