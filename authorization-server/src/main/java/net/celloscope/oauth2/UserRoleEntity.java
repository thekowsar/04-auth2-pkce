package net.celloscope.oauth2;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="userrole")
public class UserRoleEntity {

    @Id
    @Column(name = "userRoleoid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userRoleOid;

    @Column(name="userid")
    private String userId;

    @Column(name="roleid")
    private String roleId;

}
