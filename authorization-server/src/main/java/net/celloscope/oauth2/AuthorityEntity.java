package net.celloscope.oauth2;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="Authority")
public class AuthorityEntity {

    @Id
    @Column(name = "authorityoid")
    private Integer authorityOid;

    @Column(name="authorityname")
    private String authorityName;

}
